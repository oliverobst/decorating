function [DIMENS,RMSE1,RMSE2,CHANCE] = evaluate(name,N=500,theta=1.0,I=1:2,step=10)

% name : file name prefix (string)
% N : number of reservoir neurons
% theta : RMSE threshold for dimension reduction
% I : indexes of interesting input components
% step : step width

load(strcat(name,'.in'));
Org = Data(:,1:step:end);
In = Org(I,:);
len = columns(In);

[Out2,Err,A,J,Y,W,X] = predict(Org,0,N,theta,0,I);

x0=X(:,1);
Out1 = compute(W,x0,len-1)(I,:);

RMSE1 = rmse(In,Out1);
RMSE2 = rmse(In,Out2);

DIMENS = columns(J);
CHANCE = rmse(In,In(randperm(len)));
save(strcat(name,'.out'),'-binary','In','Out1','Out2','A','J','Y','W','X','DIMENS','RMSE1','RMSE2','CHANCE');

diagram = figure(1);
hold off;
plot(Data(1,:),Data(2,:),'Color','black');
hold on;
plot(Out1(1,:),Out1(2,:),'Color','blue');
plot(Out2(1,:),Out2(2,:),'Color','red');
hold off;
print(diagram,'-deps','-color',strcat(name,'.eps'));

endfunction