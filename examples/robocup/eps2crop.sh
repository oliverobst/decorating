#!/bin/sh

for name in *.eps
do
	name=${name%.eps}
	epstopdf $name.eps
	pdfcrop $name.pdf
done
