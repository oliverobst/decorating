Wf = [1 1;
      1 0];
xf = [1 0]';

[V,D] = eigs(Wf);
%V =
%   0.52573  -0.85065
%  -0.85065  -0.52573
%
%D =
%  -0.61803         0
%         0   1.61803

phi = (1+sqrt(5))/2;
W = [0 1/sqrt(5) -1/sqrt(5);
     0 phi 0;
     0 0 -1/phi];
x = [0 phi -1/phi]';
% start with 0 because of delay
    
Fibonacci = [0 fibonacci(30)]
do [Out,Err,A,J,Y,W,X] = predict(Fibonacci,more=0,N=30,NRMSE=0.001);
until (columns(A)==2); full(J)
