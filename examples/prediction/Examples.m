% Linear Recurrent Neural Networks

% y = f(t) = F(x) where x = f(t-d) for some step d

% recursively defined functions, computed by recurrent neural networks
% only with linear activation, but possibly several auxiliary units
% one argument and one return value, first or last component contains result

% (c) Frieder Stolzenburg


% constant 1 and linear function a*t+b
a = 2; b = -1;
f0 = [1 1; 0 1];
x0 = [b a]';

% exponential function exp(t)
f1 = [e];
x1 = [1];

% periodic function
l = 3; % period length
f2 = shift(eye(l),1);
x2 = (l:-1:1)';

% identity and sum of naturals
m = 3; % sum nesting level
f3 = tril(ones(m)); % lower triangle ones matrix
x3 = eye(m,1);

% powers of t in consecutive rows
m = 6; % number of different exponents 
f4 = pascal(m,-1); % Pascal's triangle
x4 = eye(m,1); % initialize with 0^m

% powers of a as output of power series
m = 10; % accuracy of power series
a = 2; % basis of powers
f5 = zeros(m+1,m+1);
f5(1:m,1:m) = pascal(m,-1);
f5(m+1,1:m) = log(a).^(0:m-1)./factorial(0:m-1);
x5 = eye(m+1,1); % initialization

% cosine wave by Euler's formula
n = 100; % period length
a = 2*pi/n; % angular frequency
f6 = [exp(a*i) 0 0; 0 exp(-a*i) 0; 1/2 1/2 0];
x6 = [1 1 0]';

% ellipse by trigonometric addition theorems: a1*cos/a2*sin(2πf*t+p)
a = [1 0.1]; % amplitude
f = 1; % frequency
p = 0; % phase shift
s = 0.01; % step width
d = 2*pi*f*s; % rotation angle
x7 = [a(1)*cos(p) a(2)*sin(p)]'; 
f7 = [cos(d) -sin(d)*a(1)/a(2); sin(d)*a(2)/a(1) cos(d)]; % 2D rotation matrix
