function [s,f,x] = series(s,m=1,d=1)
% generate arithmetic series of arbitrary order from a given series s
% for n input values, implicitly a polynomial of degree n-1 is taken

% s : arithmetic series
% f : function matrix
% x : difference series initialisation
% m : extrapolation for m more steps
% d : rows of difference series shown

n = length(s);
if (d==0)
  d=n; % show all rows if d==0
endif
x = diag((-1).^(0:n-1))*pascal(n,1)*s'; % initialisation
f = triu(ones(n))-triu(ones(n),2); % diference series = Jordan block
s = compute(f,x,n+m-1)(1:d,:);

endfunction
