addpath("library")

global N=200;
global limit=762;       #(limit=700 - take only last 700 values from data of 3 years)
select=0;               #0=take only one stock, 1=take every listed stock within file 
global makeplot= true;

%change filepath according to selected prediction form

global pathstock = "stock/";             #original stock 
pathstock = glob('stock/*.csv');

#global pathstock = "stockfund/";         #CSV files with fundamental values
#pathstock = glob('stockfund/*.csv');