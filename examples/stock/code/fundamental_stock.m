prep_prediction;

#read in data
stockdata=fopen(pathstock{28});
      Dat= textscan(stockdata,'%s %s %f %f %f %f %f %d %f %f %f %f %f %f', 'Delimiter', ',','headerlines',1);
      name = dir(pathstock{28});
      stock_name=name.name 
      
      price=transpose(Dat{6});
      KGV=transpose(Dat{12});                    #select fundamental value
      %KCV=transpose(Dat{13});
      %DIV=transpose(Dat{14});
      data(2,1:762) = price;
      data(3,1:762) = KGV; 
      %data(4,1:762)= KCV;
      %data(5,1:762)= DIV;
      fclose(pathstock{28});
      
  data(1,1:762)=datenum(Dat{:,2});       #if fundamental values need to be read in
  lim=(numel(data(2,:))+1)-limit
  data=data(:,lim:end);  
  
 #split data into training and testing
  limit_train=round(numel(data(2,:))*0.8);
  training_data=data(:,1:limit_train);
  testing_data=data(:,limit_train+1:end);
 
  S=training_data(2:end,:);
  T=testing_data(2:end,:);

 #determine training and testing size
  size_training=size(training_data);
  size_training=size_training(:,2);
  size_testing=size(testing_data);
 
 #set M for prediction for m more steps
  M=size_testing(:,2);     #m corresponds to size of testing_data for evaluation


  
  
  #predict stock price
  
 threshold=50000000;       #set threshold to determine best model
 count=0                   #count improved models
 
 for i=1:20
       [Out,Err,A,J,Y,W,X]=predict(S,M,N,theta=0,epsilon=Inf,delta=0,Range=1);
        RMSE_training=Err;
        predicted=Out(:,size_training+1:end);
        RMSE_testing=nrmse(T,predicted,1,1);
          if(RMSE_testing<threshold)
           count=count+1
            threshold=RMSE_testing;
            Out_f=Out;
            A_f=A;
            J_f=J;
            Y_f=Y;
            W_f=W;
            X_f=X; 
            
            #evaluate PrNN model 
            predicted_f=Out_f(:,size_training+1:end);
            model= Out_f(:,1:size_training);
            RMSE_training_f=Err;                            #RMSE for training data
             
            RMSE_testing_f=nrmse(T(1,:),predicted_f,1,0);   #RMSE for testing data
          endif  
   endfor

   
 RMSE_training_f
 RMSE_testing_f


#make plots
 dates_test=testing_data(1,:);
 dates_train=training_data(1,:); 

 if(makeplot)  
        plot(dates_train,model(1,:), ";Geschaetzter \nSchlusspreis (Trainingsdaten);", 'linestyle',  '-' , 'color', [0.53,0,0],
             dates_train,S(1,:), ";Schlusspreis (Trainingsdaten);", 'linestyle', '-' , 'color', [0,0,0.53],
             dates_test,predicted_f(1,:), ";Prognostizierter \nSchlusspreis (Testdaten);", 'linestyle',  '-' , 'color', [0.9,0.4,0],
             dates_test,T(1,:), ";Schlusspreis (Testdaten);", 'linestyle', '-' , 'color', [0,0.5,0.8]);
             legend ("location", "northeastoutside")
             datetick("x","yyyy-mm","keeplimits")  
  endif