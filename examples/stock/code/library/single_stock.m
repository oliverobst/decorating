function[Out,A,J,Y,W,X,RMSE_training_f,RMSE_testing_f,predicted_f]=single_stock(S,T,size_training,M,testing_data,training_data)
prep_prediction;

threshold=1000    #set threshold to find best model
count=0
    
   for i=1:20
      [Out,Err,A,J,Y,W,X]=predict(S,M,N,theta=0,epsilon=Inf,delta=0,Range=0);  #build model and predict m more steps
       predicted=Out(:,size_training+1:end);
       RMSE_testing=nrmse(T,predicted,1,0);
          if(RMSE_testing<threshold)     #select best model
           count=count+1
            threshold=RMSE_testing;
            Out_f=Out;
            A_f=A;
            J_f=J;
            Y_f=Y;
            W_f=W;
            X_f=X; 
            
            #evaluate PrNN model 
            predicted_f=Out_f(:,size_training+1:end);
            model= Out_f(:,1:size_training);
            
            RMSE_training_f=Err;
            RMSE_testing_f=nrmse(T,predicted_f,1,0);
           endif  
   endfor
 RMSE_training_f
 RMSE_testing_f
 %make plots
 dates_test=testing_data(1,:);
 dates_train=training_data(1,:);


     if (makeplot) 
       plot(dates_train,model, ";Geschaetzter \nSchlusspreis \n(Trainingsdaten);", 'linestyle',  '-' , 'color', [0.53,0,0],
             dates_train,S(:,:), ";Schlusspreis \n(Trainingsdaten);", 'linestyle', '-' , 'color', [0,0,0.53],
             dates_test,predicted_f, ";Prognostizierter \nSchlusspreis \n(Testdaten);", 'linestyle',  '-' , 'color', [0.9,0.4,0],
             dates_test,T(:,:), ";Schlusspreis \nTestdaten);", 'linestyle', '-' , 'color', [0,0.5,0.8]);
       lgd=legend ("location", "northeastoutside");
       set(lgd,  'fontsize', 9);
       datetick("x","yyyy-mm","keeplimits")
    endif 
endfunction