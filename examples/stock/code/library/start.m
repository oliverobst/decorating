function x = start(N)
% compute balanced start vector

x = ones(N,1)/max(sqrt(N),1);

endfunction