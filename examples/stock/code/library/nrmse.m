function result = nrmse(In,Out,Mode=1,Flag=0)
% normalised root mean square error

% In : given sequence
% Out : computed sequence
% Mode : kind of normalisation 
% Flag : 0 = one averaged value or 1 = per row

result = (Out-In).^2;
switch (Mode)
  case 0  % without normalisation
    result = sum(result,2);
  case 1 % wrt. number of elements
    result = mean(result,2);
  case 2 % divided by variance
    result = mean(result,2)./var(In,0,2);
endswitch

if (Flag)
  result = mean(result);
endif

result = sqrt(result);
endfunction
