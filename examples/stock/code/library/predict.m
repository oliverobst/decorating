function [Out,Err,A,J,Y,W,X] = predict(S,M=0,N=-1,theta=0,epsilon=Inf,delta=0,Range=0)
% predict next values of a given input sequence
% S : input sequence (time series)
% M : prediction for M more steps
% N : reservoir size (number of neurons) (-1 = same number of equations and unknowns)
% theta : threshold for NRMSE (0 = no dimensionality reduction)
% epsilon : desired maximal deviation of eigenvalues from absolute value 1 (Inf = no cleaning)
% delta : minimal distance between eigenvalues (0 = no clustering)
% Range : indices of relevant input/output components (0 = take all components)

% Out : output sequence (predicted)
% Err : NRMSE ouf predicted output
% A : output matrix (for reduced number of dimensions)
% J : Jordan matrix (for dynamics of reduced version)
% Y : network dynamics state sequence
% W : overall original learned transition matrix [Wout; Win Wres]
%   Wout : learned output weights [Wout]
%   Wir : input and reservoir weight matrix [Win Wres]
% X : original input and reservoir state sequence [S; R]


% INITIALISATION
n = columns(S)-1; % sequence length
d = rows(S); % number of inputs, often = 1
if (N<0)
  N = n-d;
endif


% LEARN OUTPUT WEIGHTS

% network initialization (randomly)
W = zeros(d+N);
Index = d+(1:N);
W(Index,1:d) = randn(N,d)/sqrt(d); % balanced input weights [Win]
X = zeros(d+N,n+1); % input and reservoir state sequence
W(Index,Index) = reservoir(N); % Wres
X(Index,1) = start(N);

% drive given input through reservoir (input receiving mode)


X(:,1:n+1) = compute(W,X(:,1),0,S);
% learn output weights
Yout = S(:,2:n+1); % predicted sequence
warning('off','Octave:singular-matrix');
W(1:d,:) = Yout/X(:,1:n); % output weights
warning('on','Octave:singular-matrix');


% REAL JORDAN DECOMPOSITION

% preparation
if (Range==0)
  Range = 1:d;
endif
[Centr,Multi] = cluster(eig(W),delta); % clustering eigenvalues
[Lambda,Multi] = clean(Centr,Multi,epsilon); % omit small eigenvalues  

% construct Jordan matrix and determine mapping matrix
[J,N,K] = jormat(Lambda,Multi);
Y = compute(J,start(N),n); % output generating mode
A = (X/Y)(Range,:);


% DIMENSION REDUCTION

% initialisation
In = X(Range,:);
Out = A*Y; % predicted sequence
Err = nrmse(In,Out); % original error

if (Err<theta)

  % compute error for each component and sort them in ascending order
  Err1 = zeros(K,1);
  pos = 1; % position in Jordan matrix
  for (k=1:K)
    m = Multi(k);
    Index = [1:pos-1 pos+m:N];
    J1 = J(Index,Index);
    Y1 = compute(J1,start(N-m),n);
    A1 = (X/Y1)(Range,:);
    Out1 = A1*Y1;
    Err1(k) = nrmse(In,Out1);
    pos += m;
  endfor
  
  % re-order Jordan components
  [Err1,Ord] = sort(Err1);
  Lambda = Lambda(Ord);
  Multi = Multi(Ord);
  [J,N,K] = jormat(Lambda,Multi);

  % omit components as long as error remains small enough
  for (k=1:K)
    m = Multi(k);
    J2 = J(m+1:end,m+1:end);
    N -= m;
    Y2 = compute(J2,start(N),n);
    A2 = (X/Y2)(Range,:);
    Out2 = A2*Y2;
    Err2 = nrmse(In,Out2);
    if (Err2<theta)
      Out = Out2;
      Err = Err2;
      A = A2;
      J = J2;
      Y = Y2;
    else
      break;
    endif
  endfor

endif

% PREDICTION

if (M>0)
  Out = [Out(:,1:end-1) A*compute(J,Y(:,end),M)];
endif

endfunction
