function W = reservoir(N)
% create reservoir of size N with weight matrix W and unit spectral radius,
% corresponding start vector r with unit norm

% reservoir
if (N>1)
  while (true)
    try
      W = randn(N); % Gaussian noise square matrix
      W /= abs(eigs(W,1,'lm')); % set spectral radius
      break;
    catch
      ;
    end_try_catch
  endwhile
else
  W = ones(N);
endif

endfunction
