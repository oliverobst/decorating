function[Out,A,J,Y,W,X,RMSE_training_f,RMSE_training_avg,RMSE_testing_f,RMSE_testing_avg,count]=multiple_stock(S,T,size_training,M,testing_data,training_data)
prep_prediction;

threshold=50000;
count=0
for i=1:20
       [Out,Err,A,J,Y,W,X]=predict(S,M,N,theta=0,epsilon=Inf,delta=0,Range=0);
        RMSE_training=Err;
        predicted=Out(:,size_training+1:end);
        RMSE_testing=nrmse(T,predicted,1,1);
          if(RMSE_testing<threshold)
           count=count+1
            threshold=RMSE_testing;
            Out_f=Out;
            A_f=A;
            J_f=J;
            Y_f=Y;
            W_f=W;
            X_f=X; 
            
            #evaluate PrNN model 
            predicted_f=Out_f(:,size_training+1:end);
            model= Out_f(:,1:size_training);
            RMSE_training_f=Err;
            RMSE_training_avg=nrmse(S,model,1,1);
             
            RMSE_testing_f=nrmse(T,predicted_f,1,0);
            RMSE_testing_avg=nrmse(T,predicted_f,1,1); 
          endif  
   endfor   
   
   RMSE_training_f
   RMSE_testing_f
   
 #make plots
 dates_test=testing_data(1,:);
 dates_train=training_data(1,:); 
 
#plot multiple stocks (training,testing and predicted values)
if(makeplot)
      for i=1:3
      figure(i)  
        plot(dates_train,model(i,:), ";Geschaetzter \nSchlusspreis (Trainingsdaten);", 'linestyle',  '-' , 'color', [0.53,0,0],
             dates_train,S(i,:), ";Schlusspreis (Trainingsdaten);", 'linestyle', '-' , 'color', [0,0,0.53],
             dates_test,predicted_f(i,:), ";Prognostizierter \nSchlusspreis (Testdaten);", 'linestyle',  '-' , 'color', [0.9,0.4,0],
             dates_test,T(i,:), ";Schlusspreis (Testdaten);", 'linestyle', '-' , 'color', [0,0.5,0.8]);
             legend ("location", "northeastoutside")
             datetick("x","yyyy-mm","keeplimits")  

       endfor 
   endif 
   
 endfunction