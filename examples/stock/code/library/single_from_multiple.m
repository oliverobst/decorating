function[Out,Err,A,J,Y,W,X,RMSE_training_f,RMSE_testing_f,count,Out_f]=single_from_multiple(S,T,size_training,M,testing_data,training_data)
 prep_prediction;
 
 threshold=1500;      #set threshold to choose best model within loop
 count=0              
for i=1:20
       [Out,Err,A,J,Y,W,X]=predict(S,0,N,theta=0,epsilon=Inf,delta=0,Range=0);   #build model
       
       total_data=[S T];     #combine training and testing data 
       d=rows(T);             
       n=columns(T);
       pred=zeros(d+N,n+1);     #develop matrix for predicted values
       
       pred(:,1:n+1) = compute(W,X(:,size_training),0,total_data(1:29,size_training:end)); #predict testing data
       pred=pred(:,2:n+1);
       RMSE_testing=nrmse(T(30,:),pred(30,:));
       
          if(RMSE_testing<threshold)     #choose best model 
            count=count+1;
            threshold=RMSE_testing;
            pred_f=pred;
            Out_f=[Out(30,:) pred_f(30,:)];
            A_f=A;
            J_f=J;
            Y_f=Y;
            W_f=W;
            X_f=X; 
            
            #evaluate PrNN model 
            predicted_f=Out_f(:,size_training+1:end);
            model= Out_f(:,1:size_training);
            
            RMSE_training_f=Err(30,:);           
            RMSE_testing_f=RMSE_testing; 
          endif  
   endfor   

 RMSE_training_f
 RMSE_testing_f

 #plot data
 dates_test=testing_data(1,:);
 dates_train=training_data(1,:);

  if(makeplot) 
     plot(dates_train,model, ";Geschae tzter \nSchlusspreis \n(Trainingsdaten);", 'linestyle',  '-' , 'color', [0.53,0,0],
          dates_train,S(30,:), ";Schlusspreis \n(Trainingsdaten);", 'linestyle', '-' , 'color', [0,0,0.53],
          dates_test,predicted_f, ";Prognostizierter \nSchlusspreis \n(Testdaten);", 'linestyle',  '-' , 'color', [0.9,0.4,0],
          dates_test,T(30,:), ";Schlusspreis \nTestdaten);", 'linestyle', '-' , 'color', [0,0.5,0.8]);
       lgd=legend ("location", "northeastoutside");
       set(lgd,  'fontsize', 9);
       datetick("x","yyyy-mm","keeplimits")
   endif 
 endfunction