function [Decode,Scale,Error] = princomp(In,d)
% reduce input data, given as n column vectors in m dimensions, to d dimensions
% encoding and decoding data yields autoencoding with certain error per value
% scale shows the extensions of the respective dimensions

[m,n] = size(In); % n samples in m dimensions
[U,D,V] = svd(In,0); % Decode function U ...
Decode = U(:,1:d); % ... reduced to d dimensions
Scale = diag(D); % singular values in descending order
Reduced = Decode'*In; % coded data with d dimensions
Reconstruct = Decode*Decode'; % reconstruction matrix
Out = Reconstruct*In; % reconstructing original data
Error = norm(Out-In,'fro')/(m*n); % normalized Frobenius norm

endfunction
