function [Lambda,MultiOut] = clean(Centr,MultiIn,epsilon=0)
% filter out important eigenvalues by removing
% 1. eigenvalues with negative imaginery part
% 2. eigenvalues whose absolute values deviate more than epsilon from 1

% Centr : cluster centroids
% Multi : cluster size
% epsilon : maximal deviation from absolute value 1
%   0 = ((1-epsilon)^sequence length < machine precision)

if (epsilon==0)
  epsilon = 1-nthroot(eps,length(Centr));
endif

% consider only complex numbers with:
Index = find(and(imag(Centr)>=0, % non-negative imaginery part
                 abs(abs(Centr)-1)<epsilon)); % absolute value close to 1
Lambda = Centr(Index);
MultiOut = MultiIn(Index);
MultiOut(find(imag(Lambda)>0)) *= 2;

endfunction