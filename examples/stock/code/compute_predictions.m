#run compute_prediction to calculate predictions

prep_prediction;

 #read in stock data
 [training_data,testing_data,data,stock_name]=read_in_data();
 S=training_data(2:end,:);
 T=testing_data(2:end,:);

 #determine training and testing size
 size_training=size(training_data);
 size_training=size_training(:,2);
 size_testing=size(testing_data);
 
  
 #predict m more steps
 M=size_testing(:,2);     #m corresponds to size of testing_data for evaluation

 
 
 #develop model and predict testing_data 
  switch(select)
     case 0    #single stock prediction
        [Out,A,J,Y,W,X,RMSE_training_f,RMSE_testing_f]=single_stock(S,T,size_training,M,testing_data,training_data);
    
     case 1    #multiple stock prediction
        [Out,A,J,Y,W,X,RMSE_training_f,RMSE_training_avg,RMSE_testing_f,RMSE_testing_avg,count]=multiple_stock(S,T,size_training,M,testing_data,training_data);
    
     case 2    #predict one stock from others 
        [Out,Err,A,J,Y,W,X,RMSE_training_f,RMSE_testing_f,count,Out_f]=single_from_multiple(S,T,size_training,0,testing_data,training_data);
  endswitch

   
       
