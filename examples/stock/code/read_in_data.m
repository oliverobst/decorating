function [training_data,testing_data,data,stock_name]=read_in_data()
 prep_prediction;

 #read in data
switch (select)     
  case 0     #read in single stock
      stockdata=fopen(pathstock{1});
      Dat= textscan(stockdata,'%s %f %f %f %f %f %d', 'Delimiter', ',','headerlines',1);
      price=transpose(Dat{5});
      data(2,:) = price;
      name = dir(pathstock{1});
      stock_name=name.name 
      fclose(pathstock{1}); 
      
      
 case 1      #read in every stock within folder
   stock_name=0;
    numberofstock = numel(pathstock);
    for i=1:numberofstock
       stockdata=fopen([pathstock{i}]);
       Dat= textscan(stockdata,'%s %f %f %f %f %f %d', 'Delimiter', ',','headerlines',1);
       price=transpose(Dat{5});
       data(i+1,:) = price;
       name = dir([pathstock{i}]);
       stock_name=name.name
       fclose([pathstock{i}]);
    endfor
    
      
   case 2     #read in every stock in folder
   stock_name=0;
    numberofstock = numel(pathstock);
    for i=1:numberofstock
       stockdata=fopen([pathstock{i}]);
       Dat= textscan(stockdata,'%s %f %f %f %f %f %d', 'Delimiter', ',','headerlines',1);
       price=transpose(Dat{5});
       data(i+1,:) = price;
       name = dir([pathstock{i}]);
       stock_name=name.name
       fclose([pathstock{i}]);
    endfor
    data([31,2],:)=data([2,31],:);      #set position of needed stock to last position
endswitch

 #set date
 data(1,:)=datenum(Dat{:,1});  
 lim=(numel(data(2,:))+1)-limit
 data=data(:,lim:end);  
  
 #splitting data into training and testing
 limit_train=round(numel(data(2,:))*0.8);
 training_data=data(:,1:limit_train);
 testing_data=data(:,limit_train+1:end);
 
endfunction