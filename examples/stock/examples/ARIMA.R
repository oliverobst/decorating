rm(list=ls(all=TRUE))
library ('ggplot2')
library('forecast')
library('tseries')
library('zoo')
library('hydroGOF')  #for nrmse
library('TSA') 

#read in data (insert stock name)
  sdata=read.csv('C:/Users/litz_/Desktop/stock_prediction/stock/Airbus.csv',header=TRUE,sep=",",stringsAsFactors=FALSE)
  sdata$Date = as.Date(sdata$Date)                  #set Date in according format 'Date'
  
  sdata=sdata[1:nrow(sdata),]
  
#seperate data into training- and testdata  
  training_size = ceiling(0.80 * nrow(sdata))           #set limit for training data

  #training data
  training_data=sdata[1:training_size,]               #create new data frame and fill with training data
  ts_data = ts(training_data[,5])                     #declare of closing price as a time series
  training_data$timeseries = ts(ts_data)              #add time series to training_data 

  #testing data 
  testing_data=sdata[(training_size+1):nrow(sdata),]  #create new data frame and fill with testing data
  ts_data = ts(testing_data[,5])                      #set closing price as time series
  testing_data$timeseries = ts(ts_data)               #add time series to training_data 
  
  
#build ARIMA/SARIMA model
  
  arima_model=auto.arima(training_data$timeseries, trace=TRUE, approximation=FALSE) #develop arima_model according to timeseries

  tsdisplay(residuals(arima_model),lag.max=15)                                  #check residuals
  training_data$fit=arima_model$fitted                                          #save estimated training data

#predict testing data
  predicted=forecast(arima_model,h=nrow(testing_data))                         #predict testing data
  predicted_tmp=as.numeric(predicted$mean)                                     
  testing_data$Prediction=predicted_tmp

  ggplot()+ 
    labs(title=(expression(atop("ARIMA (2,1,2)",paste(phi[1],'=0.9764, ',phi[2],'=-0.9366, ',theta[1],'=-0.9535, ', theta[2],'=0.8747, ','c=0.1741')))), x="Zeit in t", y="Schlusspreis in ???", col="") +
    theme(plot.title=element_text(hjust=0.5)) +
    geom_line(data=training_data,  aes(x=Date, y = Close, col = "Schlusspreis \n(Trainingsdaten)")) + 
    geom_line(data=training_data,  aes(x=Date, y = fit, col = "Geschätzter \nSchlusspreis \n(Trainingsdaten)\n"))+  
    geom_line(data=testing_data, aes(x=Date, y = Close, col = "Schlusspreis \n(Testdaten)\n")) +
    geom_line(data=testing_data, aes(x=Date, y = Prediction, col = "Prognostizierter \nSchlusspreis \n(Testdaten)\n")) +
    scale_x_date(date_labels = "%b %y", date_breaks = "6 months")+
    scale_color_manual(values = c("#8B0000","#FA8072","#6495ED","#00008B"))
  
  
#calculate RMSE for training and testing data
  RMSE_train=rmse(training_data$timeseries,training_data$fit)
  RMSE_train
  
  RMSE_test=rmse(testing_data$timeseries,testing_data$Prediction)
  RMSE_test

  
