\documentclass[runningheads,a4paper]{llncs}

\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage{times}
\setcounter{tocdepth}{3}
\usepackage[pdftex]{graphicx}

%\setlength{\oddsidemargin}{0.9cm}
%\setlength{\evensidemargin}{0.9cm}
%\setlength{\textwidth}{14cm}

% todo notes
\usepackage{todonotes} % disable in final version

\begin{document}

\mainmatter % start of an individual contribution

%\toctitle{Lecture Notes in Computer Science}                                                                        
%\tocauthor{Authors' Instructions}                                                                                   

% first the title is needed                                                                                          
\title{Analysing Soccer Games with Clustering and Conceptors}

% a short form should be given in case it is too long for the running head                                           
% \titlerunning{}

\author{Olivia Michael\inst{1} \and Oliver Obst\inst{1}
	\and Falk Schmidsberger\inst{2} \and Frieder Stolzenburg\inst{2}}
%                                                                                                                    
%\authorrunning{} % abbreviated author list (for running head)                               
%                                                                                                                    
% list of authors for the TOC (use if author list has to be modified)                                             
%\tocauthor{}
%                                                                                                                    
\institute{
	Centre for Research in Mathematics, Western Sydney University, Locked Bag 1797, Penrith NSW 2751, Australia\\
	EMail: \email{o.obst@westernsydney.edu.au} \and
	Department of Automation and Computer Sciences, Harz University of Applied Sciences, Friedrichstr. 57-59, 38855 Wernigerode, Germany\\
	EMail: \email{fstolzenburg@hs-harz.de}
	}

\maketitle
\todo{Shall we mention all four e-mail addresses?}

\begin{abstract}
%\todo[inline]{Who does it?}
% What
We present a new approach for identifying situations and behaviours, \emph{moves}, from soccer games in 2D Simulation League.
%       predicting the development of a soccer game, from a given situation in the match.
% Why
Being able to identify key situations and behaviours are useful capabilities for analysing soccer matches, anticipating opponent behaviours to aid selection of appropriate tactics, and also as a prerequisite for automatic learning of behaviours and policies.
% How
To support a wide set of strategies, our goal is to identify situations from data, in an unsupervised way without making use of pre-defined soccer specific concepts such as ``pass'', or ``dribble''. The recurrent networks we use in our approach act as a high-dimensional projection of the recent history of a situation on the field. Similar situations, with similar histories, are found by clustering of network states. The same networks are also used to learn conceptors, lower-dimensional manifolds that describe trajectories through a high-dimensional state space that enable situation-specific predictions from the same neural network.
% Results
With the proposed approach, we can segment games into sequences of situations that are learnt in an unsupervised way, and learn conceptors that are useful for prediction of the near future of the respective situation. 
% Significance

\keywords robotic soccer, logfile analysis, clustering, recurrent neural networks, conceptors, information retrieval.                                                                                               
\end{abstract}                                                                                                      


\section{Introduction}
%\todo[inline]{Oliver}

Some of the recent achievements of AI systems~\cite{FBC+10,MKS+15,SHM+16,BS17} have in common that they used some form of machine learning as a key component, and that their performance can be immediately compared against the performance of a human player. Teams in the Soccer Simulation leagues play against each other, a comparison against humans is not a part of the competition\footnote{To our knowledge, experimental games in the earlier years of RoboCup against a team of humans using joysticks to control players were easily won by computer programs. It should be noted, however, that the soccer simulation was not designed to be played by humans, and how human players interface with the simulation will obviously affect performance to a large degree.}.
As such, it is impossible to say how well simulation league teams play in terms of potential human performance. It is, however, possible to observe continuing progress in performance of champion teams~\cite{GR11,GFG17}. In this paper, we propose a new method for automatically analysing soccer matches, a step we see as a prerequisite to learning behaviours and strategy for a team.

\subsection{Motivation}
%\todo[inline]{Oliver}

A capability to model and describe possible situations and interactions in a game of autonomous robots has benefits for training or programming the team. With a meaningful vocabulary of situations, appropriate actions and strategies can be implemented, and together with a deeper analysis of past soccer matches, likely future situations can be better predicted. 

In the context of RoboCup soccer, several efforts have been made to support the
description of situations, interactions between players, or sequences of actions
and situations. In \cite{Mur04,MOS01b}, for example, UML statecharts are used to
describe plans of individual agents and the team, with the goal to manually
specify team behaviour. An information-theoretic approach for the analysis of
interactions has been developed in~\cite{CLW+17}; this approach is able to
identify dynamic relationships between players of a team and its opponents. In
contrast to the UML statecharts approach, the information-theoretic analysis of
interactions is model-free, and does not rely on predefined, soccer-specific
concepts (e.g., pass or dribble). One possible output of this approach are
interaction diagrams that can be used to analyse strengths of specific players
in a given match.

To be able to analyse, communicate, and learn team behaviour, it would be beneficial to use an approach that also  does not rely on predefine soccer knowledge, but for example is able to learn how to describe situations or plans from recorded logfiles of games. This way,   the 
analysis will rely on behaviours frequently occurring in past games instead of behaviours that may be familiar to the developer from, e.g., human soccer matches, or similar prior knowledge that is required when using manually specified plans.

An obvious application of identifying frequent behaviours in games is the analysis of logfiles, to be used for programming to improve team performance, or for automated commentator systems.  A challenge for the goal of identifying behaviours is to solve the apparently simple questions what constitutes a behaviour, and what ``situation'' means, in a soccer match. Even though the simulation naturally segments a game into steps of 100ms, such a fine grained segmentation does not appear to be practical to compare longer sequences of actions, or ``moves''.   

Being able to predict likely moves from a given situation is a useful first step towards learning behaviours for soccer players. 
While a few successful teams employed machine learning techniques, most notably reinforcement learning in, e.g.,~\cite{RM03,GR06,GRT09,GR16}, or planning with MDPs in~\cite{BWC15}, use of machine learning has been limited to components like individual skills or behaviours. Learning most of the teams behaviour is quite challenging due to the large number of possible actions each player can choose from. The large number of possible moves is also a challenge for computer programs created to play the game of Go, and the recent  success in this field~\cite{SHM+16}, for example, is based on so-called ``policy networks''. A successful prediction of expert moves from a given situation on the Go board has been a first step in this success story. To take this first step in robotic soccer, the goal of our project is to eventually be able to identify both ``actions'' and ``situations'', with the application to learn probability distributions of the form $p(\text{action} | \text{situation})$. 

\subsection{Notions}
%\todo[inline]{Frieder}

When we analyse time-series data by machine learning techniques, we shall
distinguish the raw observed data from its reflection in a world model. In our
case, on the one hand, we have the soccer simulation data given by logfiles
representing more or less real-world data. On the other hand, we have a world
model which will be loaded into an artificial recurrent neural network,
explained later in some detail.

A soccer simulation game in the RoboCup 2D Simulation League lasts
10\,mins in total, divided into 6000 cycles where the length of each cycle is
100\,ms. The central SoccerServer \cite{CDF+03} controls every virtual game with
built-in physics rules. Logfiles comprise information about the game, in
particular about the current positions of all players and the ball including
their velocity and orientation for each cycle, which we call \emph{world state}
in the following.

The challenge in the Simulation League is to derive for all possible world
states, possibly including their state history, the best possible action to
perform for each player, understood as intelligent agent in this context
\cite[Sect.~~2]{RN09}. An \emph{action} in our case might be kick, dash (used
to accelerate the player in direction of its body), turn (change the players
body direction), and others more. Each action is executed in the actual cycle,
but its effect may hold on for a longer period of time.

For modelling the time-series induced by the logfile, we use recurrent neural
networks of a very simple form \cite{Jae07}. They consist of a number of input
neurons, containing the world state information of a given cycle. The input
neurons are connected with a reservoir usually consisting of hundreds of neurons
in a hidden layer. All neurons in the reservoir are connected randomly among
each other, at least initially. At each time point, an output signal is read out
by further connections from the reservoir to one or more output neurons. By this
procedure, each world state is reflected by a \emph{reservoir state}
corresponding to the same cycle time.

A fundamental condition for recurrent neural networks to be useful in learning
tasks is the so-called echo state property: Any random initial state of a
reservoir is forgotten, such that after a washout period the current network
state simply is a function of the driver \cite{Jae14}. We can use the series of
reservoir states for predicting future development of the game. Thus we
implicitly compute a probability distribution for the next world state or
action. In order to reach this goal, we partition the game into different moves
by clustering methods \cite[Sect.~ 6~and~7]{Agg15}. A \emph{move} in this
context means a pattern of similar states. It is derived from a sequence of
consecutive reservoir states which might occur several times during the whole
game but it means the respective series of world states.

The idea is that each move corresponds to a certain behaviour such as e.g.
kicking a goal. When a recurrent neural network is actively generating or is
passively being driven by different dynamical patterns, its reservoir states are
located in different regions of the multi-dimensional neural state space,
characteristic for that pattern. A move thus corresponds to a specific concept
which can be captured by a \emph{conceptor} \cite{Jae14}. They allows us to
analyse and identify moves and finally to predict the development of the game.
This is the main objective of this paper.

\subsection{Overview}
\todo[inline]{Olivia}

structure of the paper

\section{Methods}

\subsection{Data Preparation}

In order to prepare the data needed, we use recorded log files from matches between two teams. The main data set we will be using throughout this paper is a match between the teams Gliders2012~\cite{POWH12} and MarliK~\cite{TNV+12}. For more detailed experiments, we also created a data set of 101 games, run using the teams Helios2016~\cite{ANH+16} and Gliders2016~\cite{PWOJ16}. Each logfile contains the ``state of the world'', ground truth information sampled every 100ms, which includes the positions of the players and the ball in x and y coordinates, as well as the speed and the stamina of the players.
 
The soccer simulator records games in a binary format, which we converted to text using the tool {\tt rcg2txt}, from the librcsc library~\cite{AS11}. The resulting data were then converted from text to CSV (Comma Separated Values) files using a custom python script. For the work in this paper, we only made use of player and ball positions (23 objects with x- and y-positions, i.e., 46 values for each step of the simulation), and augmented these values with the distances between each player and the ball (22 additional values), so that the final datasets consisted of 68 columns\todo{are we still doing this? Text below seems to say we're not (i.e., 46 cols).}. Each match lasts 6000 steps (10mins in real time), but in some situations game time may be stopped, which can lead to extra steps. On average, we recorded close to 6500 game states per match. 

\subsection{Recurrent Neural Networks with Conceptors}
%\todo[inline]{Frieder}

The world model of the time-series data is loaded into a recurrent neural
network. Following the lines of \cite[Sect.~3]{Jae14}, a (discrete-time)
\emph{recurrent neural network} consists of (a) $N^\mathrm{in}$ input neurons,
(b) a reservoir, that is a set of $N$ recurrently connected neurons, and (c)
$N^\mathrm{out}$ output neurons (cf. Fig.~\ref{fig:network}). The connections between
neurons have weights which may be comprised in weight matrices: $W^\mathrm{in}$
is the $N \times N^\mathrm{in}$ input weight matrix, $W$ the $N \times N$
reservoir matrix, and $W^\mathrm{out}$ the $N^\mathrm{out} \times N$ readout
matrix. The network operates in discrete time steps $n=0,1,2,\dots$ according to
the following update equations:

\begin{eqnarray*}
x(n+1) & = & \tanh(W \cdot x(n) + W^\mathrm{in} \cdot p(n+1) +b)\\
y(n) & = & W^\mathrm{out} \cdot x(n)
\end{eqnarray*}

Here $p(n)$ denotes the input (training) signal, $x(n)$ the reservoir state, and
$y(n)$ the target signal. $b$ is an $N \times 1$ bias vector. Like the input
weights, it is fixed to random values. Due to the commonly used non-linear
activation function $\tanh$, the reservoir state space is reduced to the
interval $(-1;+1)$. A time-series may represent a move, which can be learned by
so-called conceptors as follows: The sequence of world states representing the
move is stored into the reservoir such that it can generate the driven responses
$x(n)$ even in the absence of the driving input. For this, ridge regression may
be employed. In addition, in order to retrieve the output pattern individually,
the reservoir dynamics is restricted to the linear subspace characteristic for
that pattern. This is done by special matrices called conceptors. For more
details the reader is referred to \cite{Jae14}.

\begin{figure}
\includegraphics[width=\textwidth]{fig/net.pdf}
\caption{Recurrent neural network capturing the world model.}\label{fig:network}
\end{figure}

For the analysis of soccer games, different moves are associated with
conceptors. However, we usually do not know in advance which are the moves of
the whole game. Thus we first have to establish a repertoire of moves that form
the whole game. We want to identify and learn moves by clustering methods, which
is explained in the next section. 

\subsection{Clustering of Games in Moves}
%\todo[inline]{Falk}

From the soccer logfile we extract the positions of all players and the ball
for each cycle of the game and store them, normalized between -1 and 1,
in world states.
In the example in Fig.~\ref{fig:gamelogfileviz} all world states from logfile
MarliK\_1-vs-Gliders2012\_3 are drawn for the ball and the two goalies.
To identify the sequences of world states that represents moves leading to
similar game situations we compute cluster of similar reservoir states.
Therefor we use the actual world state $p(i)$ to compute the actual reservoir
state $x(i)$ foreach cycle of the game. 
Assuming, that similar reservoir states represents similar moves in the game, a
clustering of all reservoir states $x(i)$ is performed. We use the method X-means clustering \cite{PellegM00} to derive the clusters and the number of clusters.
With the cluster elements we can identify the moves leading to this game situation.

\begin{figure}
\fbox{
\includegraphics[width=\textwidth]{fig/PositionsP1P2Ball.pdf}
}
\caption{Position visualization of the two goalies and the ball.}\label{fig:gamelogfileviz}
\end{figure}


\section{Results}
%\todo[inline]{Falk}

In our approach we extract the 6493 world states from the logfile
MarliK\_1-vs-Gliders2012\_3 and store these patterns in a into a recurrent neural
network with 46 input neurons, 600 neurons in the reservoir and 46 output neurons.
We compute the reservoir states $x(i)$ foreach worldstate and cluster them with the
X-Means algorithm of the data mining tool WEKA 3.8 \cite{hall09} \cite{Frank2016}.
With the maximum cluster count of 100 and a maximum of 5 iterations as parameters,
we get 64 clusters. Each resulting cluster can be associated to one or more sequences
of worldstates (moves). Figure \ref{fig:clusterexample} show all 5 moves of cluster 5
for all players and the ball and Figure \ref{fig:clusterexample2} all of cluster 41.

With the now identified clusters and moves it is possible to compute the conceptors
for this game situations and these conceptors let us predict the next worldstates in
similar situations in de game.
With the resulting conceptor of the reservoir filled with all worldstates, we
can predict a simplified preview of the entire game,
ref. Fig. \ref{fig:gameprediction} (only goalies and ball are drawn).


\begin{figure}
\fbox{
\includegraphics[width=\textwidth]{fig/clusterexample.pdf}
}
\caption{All patterns referring to cluster 5.}\label{fig:clusterexample}
\end{figure}

\begin{figure}
\fbox{
\includegraphics[width=\textwidth]{fig/clusterexample2.pdf}
}
\caption{All patterns referring to cluster 41.}\label{fig:clusterexample2}
\end{figure}

\begin{figure}
\fbox{
\includegraphics[width=\textwidth]{fig/GamePredictionG1G2Ball.pdf}
}
\caption{Prediction of an entire game.}\label{fig:gameprediction}
\end{figure}


\section{Discussion and Final Remarks}
\todo[inline]{Frieder}

summary

future work

\subsubsection*{Acknowledgements.}
The research reported in this paper has been supported by the German Academic
Exchange Service (DAAD) by funds of the German Federal Ministry of Education and
Research (BMBF) in the Programmes for Project-Related Personal Exchange (PPP)
under grant no.~57319564 and Universities Australia (UA) in the
Australia-Germany Joint Research Cooperation Scheme under grant no. \todo{grant
no. in Australia} within the project \emph{\underline{De}ep
\underline{Co}nceptors for Tempo\underline{r}al D\underline{at}a
M\underline{in}in\underline{g}} (Decorating).

%\afterpage{\clearpage}

\bibliographystyle{splncs03}
\bibliography{ccrefs}

\end{document}

