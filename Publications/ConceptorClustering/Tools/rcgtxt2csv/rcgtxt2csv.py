import argparse
import re

parser = argparse.ArgumentParser()

parser.add_argument('infile', nargs='+', type=argparse.FileType('r'), help='name(s) of inputfile(s), [infile].txt')
parser.add_argument('-o', '--outfile', dest='outfile', help='name of the output file (will be numbered in case of multiple inputs), defaults to [infile].csv', metavar='FILE')
args = parser.parse_args()

# print(args.infile[0].name)
# print(len(args.infile))

fileno = None
if len(args.infile) > 1:
    fileno = 1

if args.outfile is not None:
    if args.outfile.endswith('.csv'):
        basename = args.outfile[:-4]
    else:
        basename = args.outfile
    
# print(args.outfile)

for infile in args.infile:
    if args.outfile is None:
        if infile.name.endswith('.txt'):
            basename = infile.name[:-4]
        else:
            basename = infile.name

    if fileno is None:
        destFile = open(basename + '.csv', 'w')
    else:
        destFile = open(basename + '-' + str(fileno) + '.csv', 'w')
        fileno = fileno + 1

#    print('Converting from ' + infile.name + ' to ' + destFile.name)
    player = re.compile('\(player . \d\d? g? ?\(position (-?\d+\.?\d*) (-?\d+\.?\d*).*?\).*?\)')

    for line in infile:
	if line[0:5] == '(Info':
            m = re.search('\(ball (-?\d+\.?\d*) (-?\d+\.?\d*)', line)
            bx = m.group(1)
            by = m.group(2)
            pc = player.findall(line)

            destFile.write(bx +',' + by)
            n = 22
            i = 0
            while i < n:
                destFile.write(',' + pc[i][0] + ',' + pc[i][1])
                i = i + 1

            destFile.write('\n')

# #		print(bx +',' + by) 
# #		print(pc)
    
    infile.close()
    destFile.close()
