%%%% first test script for conceptor learning on soccer logfile
%%%% F. Schmidsberger, Feb 28, 2017


tic

%addpath('../helpers');

if isOctave
  page_screen_output(0);
  page_output_immediately(1);
end

disp(sprintf('start program'));


%##############################################################################

%%% Experiment control
computeDistances = 0;

%##############################################################################

S = load ("MarliK_1-vs-Gliders2012_3.csv");
S1 = S(1,:);
disp(sprintf('sample 1:'));
disp(S1);

%normalize positions
samples = S./60;

if computeDistances == 1
  %compute distance ball - player
  
  SI = size(samples);
%  disp(sprintf('samples size: %g', SI));


  disp(sprintf('compute distance ball - player'));
  ROWS=SI(1,1);
  COLS=(SI(1,2) - 2)/2;
  Distances = zeros(ROWS,COLS);

  %compute euclidian distances ball <-> player
  for x = 1:COLS
    %squared euclidian x and y distances Ball <-> player;
    A = (samples(:,1:2) - samples(:,x*2+1:x*2+2)).^2;
    %euclidian distances Ball <-> player
    Distances(:,x) = sqrt( A(:,1) + A(:,2) ); 
  end

  samples = [samples, Distances];
  %compute distance ball - player end
end 

%samples = vertcat(samples,samples,samples,samples,samples, ...
%            samples,samples,samples,samples,samples);

%samples = vertcat(samples,samples,samples,samples,samples);        

%%%plot graphs
pn1 = 1; %Number of player to plot
pn2 = 12; %Number of player to plot 
Ball = samples(:,1:2);
Player1 = samples(:,pn1*2+1:pn1*2+2);
Player2 = samples(:,pn2*2+1:pn2*2+2);

%figure(1); clf;
%set(gcf, 'WindowStyle','normal');
%plot(Ball(:,1), Ball(:,2), ' *r;Ball;');
%axis([-1 1 -0.7 0.7]);
%grid;
%

%figure(2); clf;
%set(gcf, 'WindowStyle','normal');
%plot(Player(:,1), Player(:,2), ' *b;Player;');
%axis([-1 1 -0.7 0.7]);
%grid;

figure(3); clf;
set(gcf, 'WindowStyle','normal');
set (gcf, "paperorientation", "landscape")
papersize = [30, 21]/2.54;
set (gcf, "papersize", papersize)
set (gcf, "paperposition", [0.25 0.25, papersize-0.1]) 
hold on
xlabel ("Position in x direction");
ylabel ("Position in y direction");
plot(Ball(:,1), Ball(:,2), ' .r;Ball;');
plot(Player1(:,1), Player1(:,2), ' .b;Player1;');
plot(Player2(:,1), Player2(:,2), ' .g;Player2;');
axis([-1 1 -0.65 0.65]);
grid;
hold off
print ("figure3.pdf");



if computeDistances == 1
  figure(4); clf;
  set(gcf, 'WindowStyle','normal');
  hold on
  plot(samples(:,2+COLS*2+pn1), '  b;P1-Balldistance;');
  plot(samples(:,2+COLS*2+pn2), '  g;P2-Balldistance;');
  grid;
  hold off
 end
%%%plot graphs end

SI = size(samples);
pattDim = SI(1,2); % dimension of patterns
sampleCount  = SI(1,1);
disp(sprintf('dimension of patterns: %g', pattDim));
disp(sprintf('sample count: %g', sampleCount));

%##############################################################################


randstate = 1; % random seed for creating network weights
newNets = 1; % whether new raw nets are created
newSystemScalings = 1; % whether raw nets are freshly re-scaled
%reloadData = 1; % whether normalized neural-network input data are
% re-loaded
%relearn = 1; % whether model is re-trained
%makeAllDataFlag = 0; % whether normalized neural-network input data are
% re-computed from downloaded mocap source data
% (expensive)
showStates = 1; % whether to plot some reservoir states obtained during testing
showSingVals = 1; % whether to plot singular value spectra of conceptors
%showOutTraces = 1; % whether to plot outputs of some of the 61 pattern dimensions
                   % collected for the entire demo run
%showVideo = 1; % whether video is generated and shown (Matlab-internal, slow,
% does not create an externally runnable video in one of the
% standard video formats)

%%% a few constants
nP = 1; % nr of patterns; if you experiment with additional patterns
% (or subsets of the patterns used here), adapt


%%% Setting system params
Netsize = 600; % network size
NetSR = 1; % spectral radius
NetinpScaling = .8 * ones(pattDim, 1); % scaling of input weights

%NetinpScaling([5 17],1) = 0 * ones(2,1); % these two were found to "function"
% primarily as noise and are suppressed

BiasScaling = .8; % scaling of bias vector
LR = .6; % leaking rate for leaky-integration neuron model, ranges in [0 1]
% with a value of 1 meaning no integration -- the smaller, the
% more smoothing

washoutLength = 50; % how many initial updates per pattern are discarded
% in loading and Wout computation

%%% Ridge regression regularization. Set to 0 because data were so
%%% variable that no regularization was found necessary
TychonovAlpha = 0; % regularizer for  W training
TychonovAlphaReadout = 0; % regularizer for  Wout training

%%% Conceptor learning and testing
alphas = 10 * ones(1,nP); % apertures
%alphas(2) = 3;
%alphas(13) = 20;
CtestLength = 1000;

%%% numbering of motion patterns:
% 1 ExaggeratedStride 2 SlowWalk 3 Walk 4 RunJog 5 CartWheel  6 Waltz
% 7 Crawl  8 Standup  9 Getdown 10 Sitting  11 GetSeated  12 StandupFromStool
% 13 Box1  14 Box2 15 Box3

%%% setting sequence order of patterns to be displayed
%pattOrder = [10 12 2 1 4 1 6 3 9 7 8 3 13 15 14 2  5 3 2 11 ];
%%% setting durations of each pattern episode (note: 120 frames = 1 sec)
%pattDurations = [ 150 260 200 200 250 130 630 200 120 400 100 100  ...
%    250 400 300 150 670 100 150 300 ];
%%% setting durations for morphing transitions between two subsequent patterns
%pattTransitions = 120 * ones(1, length(pattOrder)-1);

%plotPicks = [3 4 25 26  28]; % which input dimensions are plotted

%%% additive state noise level inserted during testing and video generation
stateNL = 0.;

%##############################################################################


%%% Initializations

randn('state', randstate);
rand('twister', randstate);



% Create raw weights
if newNets
    if Netsize <= 20
        Netconnectivity = 1;
    else
        Netconnectivity = 10/Netsize;
    end
    WstarRaw = generate_internal_weights(Netsize, Netconnectivity);
    WinRaw = 2*(rand(Netsize, pattDim) - 0.5);
    WbiasRaw = 2*(rand(Netsize, 1) - 0.5);
end

% Scale raw weights and initialize weights
if newSystemScalings
    Wstar = NetSR * WstarRaw;
    Win =  WinRaw * diag(NetinpScaling);
    Wbias = BiasScaling * WbiasRaw;
end


%relearn
%totalDataLength = sum(pattlengths);
totalDataLength = sampleCount;
totalLearnLength = totalDataLength - nP * washoutLength;

allTrainxArgs = zeros(Netsize + 1, 0);
allTrainOldxArgs = zeros(Netsize, 0);
allTrainWtargets = zeros(Netsize, 0);
allTrainOuts = zeros(pattDim, 0);
Wtargets = zeros(Netsize,0);
patternRs = cell(1,nP);
startXs = zeros(Netsize, nP);
% collect data from driving native reservoir with different drivers
for p = 1:nP
%        disp(sprintf('    collect data %g', p));
    %patt = patts{p}; % current pattern
    patt = samples;
    
    %learnLength = pattlengths(p) - washoutLength;
    learnLength = sampleCount - washoutLength;
    
    xCollector = zeros(Netsize + 1, learnLength );
    xOldCollector = zeros(Netsize, learnLength );
    WTargetCollector = zeros(Netsize, learnLength);
    pCollector = zeros(pattDim, learnLength );
    x = zeros(Netsize, 1);
    for n = 1:(washoutLength + learnLength)
        u = patt(n,:)'; % pattern input
        xOld = x;
        Wtarget = Wstar * xOld + Win * u;
        x = (1-LR)*xOld + LR * tanh(Wtarget + Wbias);
        if n == washoutLength
            startXs(:,p) = x;
        end
        if n > washoutLength
            xCollector(:, n - washoutLength ) = [x; 1];
            xOldCollector(:, n - washoutLength ) = xOld;
            WTargetCollector(:, n - washoutLength ) = Wtarget;
            pCollector(:, n - washoutLength) = u;
            
        end
        uOld = u;
    end
    patternRs{p} = xCollector(1:end-1,:) * xCollector(1:end-1,:)'...
        / learnLength;
    
    allTrainxArgs = [allTrainxArgs, xCollector];
    allTrainOldxArgs = [allTrainOldxArgs, xOldCollector];
    allTrainOuts = [allTrainOuts, pCollector];
    allTrainWtargets = [allTrainWtargets, WTargetCollector];
    
    xCT = xCollector(1:end-1,:)';
    save -ascii xCollectorT.m xCT;
    
end

%%% compute pattern readout
disp(sprintf('compute pattern readout'));
Wout = (inv(allTrainxArgs * allTrainxArgs' + ...
    TychonovAlphaReadout * eye(Netsize + 1)) ...
    * allTrainxArgs * allTrainOuts')';
% training error
outsRecovered = Wout*allTrainxArgs;
NRMSE_readout = nrmse(outsRecovered, allTrainOuts);
disp(sprintf('mean NRMSE readout: %g   mean abs Wout: %g',...
    mean(NRMSE_readout(not(isnan(NRMSE_readout)),1)),...
    mean(mean(abs(Wout)))));

outsTrain = cell(1,nP);
wo = washoutLength;
startInd = 1;
for i = 1:nP
%    outsTrain{i} = outsRecovered(:,...
%        startInd:startInd + pattlengths(i) - wo -1)';
%    startInd = startInd + pattlengths(i) - wo;
    
    outsTrain{i} = outsRecovered(:,...
        startInd:startInd + sampleCount - wo -1)';
    startInd = startInd + sampleCount - wo;
        
end


%%% compute W
disp(sprintf('compute W'));
W = (inv(allTrainOldxArgs * allTrainOldxArgs' + ...
    TychonovAlpha * eye(Netsize)) * allTrainOldxArgs * allTrainWtargets')';
% training errors per neuron
disp(sprintf('training errors per neuron'));
NRMSE_W = nrmse(W*allTrainOldxArgs, allTrainWtargets);
disp(sprintf('mean NRMSE W: %g   mean abs W: %g ', ...
    mean(NRMSE_W), mean(mean(abs(W)))));

disp(sprintf('relearn ..... ends'));


%% % compute conceptors
disp(sprintf('compute conceptors ..... '));
Cs = cell(4, nP);
for p = 1:nP
    disp(sprintf('compute conceptor %g', p));
    [U S V] = svd(patternRs{p});
    Snew = (S * inv(S + alphas(p)^(-2) * eye(Netsize)));
    C = U * Snew * U';
    Cs{1, p} = C;
    Cs{2, p} = U;
    Cs{3, p} = diag(Snew);
    Cs{4, p} = diag(S);    
end
disp(sprintf('compute conceptors ..... ends'));





%%% test 

disp(sprintf('test ..... '));
x_CTestPLSingle = zeros(10, CtestLength, nP);
p_CTestPLSingle = zeros(pattDim, CtestLength, nP);

for p = 1:nP
  
%    disp(sprintf('test %g', p));
    C = Cs{1, p};
    x = startXs(:,p);    
    
    for n = 1:CtestLength
        xOld = x;
        if stateNL == 0
            x = (1-LR)*xOld + LR * tanh(W *  x + Wbias);
        else
            x = (1-LR)*xOld + LR * tanh(W *  x + Wbias) ...
                + stateNL * (rand(Netsize,1)-0.5);
        end
        
        x_CTestPLSingle(:,n,p) = x(1:10,1);
        x = C * x;
        p_CTestPLSingle(:,n,p) = Wout * [x; 1];
    end
end
disp(sprintf('test ..... ends'));




%% create sequence data
L = sampleCount
mus = zeros(nP,L);

%for window = 1:length(pattOrder)
%    if window == 1 % no transition
%        mus(pattOrder(1),1:pattDurations(1)) = ...
%            ones(1,pattDurations(1));
%        startT = pattDurations(1) + 1;
%    else
%        % start with transition
%        mus(pattOrder(window-1),...
%            startT:startT+pattTransitions(window-1)-1) = ...
%            (pattTransitions(window-1):-1:1) / pattTransitions(window-1);
%        mus(pattOrder(window),...
%            startT:startT+pattTransitions(window-1)-1) = ...
%            (1:pattTransitions(window-1)) / pattTransitions(window-1);
%        startT = startT + pattTransitions(window-1);
%        mus(pattOrder(window),...
%            startT:startT+pattDurations(window)-1) = ...
%            ones(1,pattDurations(window));
%        startT = startT + pattDurations(window);
%    end
%end
%mus  = smoothmus( mus );

p_CTestPLMorph = zeros(pattDim, L);
%x = startXs(:,pattOrder(1));

thisC = Cs{1,1};

x = startXs(:,1);
for n = 1:L
    xOld = x;
    if stateNL == 0
        x = (1-LR)*xOld + LR * tanh(W *  x + Wbias);
    else
        x = (1-LR)*xOld + LR * tanh(W *  x + Wbias) ...
            + stateNL * (rand(Netsize,1)-0.5);
    end
    
%    % find which mu indices are not 0
%    thstateNLismu = mus(:,n);
%    allInds = (1:nP)';
%    muNot0Inds = allInds(thismu ~= 0);
%    if length(muNot0Inds) == 1
%        thisC = Cs{1,muNot0Inds};
%    else
%        thisC = thismu(muNot0Inds(1)) * Cs{1,muNot0Inds(1)} + ...
%            thismu(muNot0Inds(2)) * Cs{1,muNot0Inds(2)};
%    end

    x = thisC * x;
    p_CTestPLMorph(:,n) = Wout * [x; 1];
end



%%% plot some reservoir states obtained during pattern-regeneration
if showStates
    disp(sprintf('plotting .... figure(5) some reservoir states obtained during pattern-regeneration'));
    figure(5); clf;
    set(gcf, 'WindowStyle','normal');
%    set(gcf,'Position', [900 150 500 120]);
    for p = 1:nP
        subplot(1,nP,p);
        plot(x_CTestPLSingle(:,:,p)');
        if p == 1
            title('some states');
        end
    end
end

%%% plot singular value spectra of conceptors
if showSingVals
  disp(sprintf('plotting .... figure(6) singular value spectra of conceptors'));
    figure(6); clf;
    set(gcf, 'WindowStyle','normal');
%    set(gcf,'Position', [1200 150 500 120]);
    for p = 1:nP
        subplot(1,nP,p);
        Csingvals = sort(Cs{3, p},'descend');
        plotLength = min([100,  length(Csingvals) ]);
        plot(Csingvals(1:plotLength), 'b');
        if p == 1
            title('C singvals');
        end
    end
end

%%%plot graphs

cBall = p_CTestPLMorph(1:2,:)';
Player1 = p_CTestPLMorph(pn1*2+1:pn1*2+2,:)';
Player2 = p_CTestPLMorph(pn2*2+1:pn2*2+2,:)';

%figure(1); clf;
%set(gcf, 'WindowStyle','normal');
%plot(Ball(:,1), Ball(:,2), ' *r;Ball;');
%axis([-1 1 -0.7 0.7]);
%grid;
%

%figure(2); clf;
%set(gcf, 'WindowStyle','normal');
%plot(Player(:,1), Player(:,2), ' *b;Player;');
%axis([-1 1 -0.7 0.7]);
%grid;

figure(7); clf;
set(gcf, 'WindowStyle','normal');
set (gcf, "paperorientation", "landscape")
papersize = [30, 21]/2.54;
set (gcf, "papersize", papersize)
set (gcf, "paperposition", [0.01 0.01, papersize-0.01]) 
hold on
xlabel ("Position in x direction");
ylabel ("Position in y direction");
plot(cBall(:,1), cBall(:,2), ' -or;Ball;');
plot(Player1(:,1), Player1(:,2), ' -ob;Goalie 1;');
plot(Player2(:,1), Player2(:,2), ' -og;Goalie 2;');
axis([-1 1 -1 1]);
grid;
hold off
print ("fig/GamePredictionG1G2Ball.pdf");


%figure(7); clf;
%set(gcf, 'WindowStyle','normal');
%hold on
%plot(cBall(:,1), cBall(:,2), ' *r;cBall;');
%plot(Player1(:,1), Player1(:,2), ' *b;cPlayer1;');
%plot(Player2(:,1), Player2(:,2), ' *g;cPlayer2;');
%axis([-1.5 1.5 -1 1]);
%grid;
%hold off

disp(sprintf('end program'));
toc 
