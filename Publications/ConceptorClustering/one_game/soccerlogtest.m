page_screen_output(0);
page_output_immediately(1);


%%%% first test script for soccer logfile
%%%% F. Schmidsberger, Feb 27, 2017

tic
disp(sprintf('start program'));

S = load ("MarliK_1-vs-Gliders2012_3.csv");
%S1 = S(1,:);
%disp(sprintf('sample 1:'));
%disp(S1);

%normalize positions
S = S./60;

%compute distance ball - player
disp(sprintf('samples size:'));
SI = size(S);
disp(size(SI));

disp(sprintf('compute distance ball - player'));
ROWS=SI(1,1);
COLS=(SI(1,2) - 2)/2;
Distances = zeros(ROWS,COLS);

%compute euclidian distances ball <-> player
for x = 1:COLS
  %squared euclidian x and y distances Ball <-> player;
  A = (S(:,1:2) - S(:,x*2+1:x*2+2)).^2;
  %euclidian distances Ball <-> player
  Distances(:,x) = sqrt( A(:,1) + A(:,2) ); 
end

SamplesWithDist = [S, Distances];



%%%plot graphs
n = 12; %Number of player to plot 

Ball = S(:,1:2);
figure(1); clf;
set(gcf, 'WindowStyle','normal');
plot(Ball(:,1), Ball(:,2), ' *r;Ball;');
axis([-1 1 -0.7 0.7]);
grid;

Player = SamplesWithDist(:,n*2+1:n*2+2);
figure(2); clf;
set(gcf, 'WindowStyle','normal');
plot(Player(:,1), Player(:,2), ' *b;Player;');
axis([-1 1 -0.7 0.7]);
grid;

figure(3); clf;
set(gcf, 'WindowStyle','normal');
hold on
plot(Ball(:,1), Ball(:,2), ' *r;Ball;');
plot(Player(:,1), Player(:,2), ' *b;Player;');
axis([-1 1 -0.7 0.7]);
grid;
hold off

figure(4); clf;
set(gcf, 'WindowStyle','normal');
plot(SamplesWithDist(:,2+COLS*2+n), '  b;P-Balldistance;');
grid;
%%%plot graphs end



disp(sprintf('end program'));
toc 
