from argparse import ArgumentParser
import re

parser = ArgumentParser()

parser.add_argument("-if", "--ifile", dest="inputFile", help="name of the intput file", metavar="FILE")
parser.add_argument("-of", "--ofile", dest="outputFile", help="name of the output file", metavar="FILE")
args = parser.parse_args()

#print(args.inputFile)
#print(args.outputFile)


srcFile = open(args.inputFile, "r") 
destFile = open(args.outputFile,"w")

player = re.compile('\(player . \d\d? g? ?\(position (-?\d+\.?\d*) (-?\d+\.?\d*).*?\).*?\)')

for line in srcFile: 
	if line[0:5] == '(Info':
		m = re.search('\(ball (-?\d+\.?\d*) (-?\d+\.?\d*)', line)
		bx = m.group(1)
		by = m.group(2)
		pc = player.findall(line)

		destFile.write(bx +',' + by)
		n = 22
		i = 0
		while i < n:
			destFile.write(',' + pc[i][0] + ',' + pc[i][1])
			i = i + 1

		destFile.write('\n')

#		print(bx +',' + by) 
#		print(pc)

srcFile.close()
destFile.close()



