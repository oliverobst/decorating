%%%% first test script for conceptor learning on soccer logfile
%%%% F. Schmidsberger, Feb 28, 2017


%%% settings
prepareData = 1; % set 1 to reload csv files


tic
if isOctave
  page_screen_output(0);
  page_output_immediately(1);
end

disp(sprintf('start program'));

if prepareData == 1
  disp(sprintf('prepare data'));
  disp(sprintf('load data/xCollectorT_cl.csv'));
  cluster = load('data/xCollectorT_cl.csv');
  cSI = sortrows([cluster(:,end), cluster(:,1)] , 1);
  %[SO, ISO] = sortrows(cSI, 1);
  %SORTED = sortrows(cSI, 1);

  disp(sprintf('load data/MarliK_1-vs-Gliders2012_3.csv'));
  S = load ('data/MarliK_1-vs-Gliders2012_3.csv');
  washoutLength = 50; % how many initial updates per pattern are discarded
  %normalize selected positions
  samples = S(washoutLength+1:end,:)./60;

  save data/samples__cluster.mat cSI;
  save data/samples__soccer.mat samples;
  disp(sprintf('prepare data end'));
end

disp(sprintf('load data'));
load data/samples__cluster.mat cSI;
load data/samples__soccer.mat samples;
disp(sprintf('load data end'));


disp(sprintf('compute plot data'));
%%%plot graphs
pn1 = 1; %Number of player to plot
pn2 = 12; %Number of player to plot 
Ball = samples(:,1:2);
Player1 = samples(:,pn1*2+1:pn1*2+2);
Player2 = samples(:,pn2*2+1:pn2*2+2);

cCount = 1 + max(cSI(:,1));
%cCount = 6;
collector = cell(cCount, 3);

s_cSI=size(cSI);
rows = s_cSI(1,1);
disp(sprintf('sample count: %g, cluster count %g', rows, cCount));


%collect data for each cluster
%disp(sprintf('collect data for each cluster'));

bTemp = [0,0];
pLeftTemp = [0,0];
pRightTemp = [0,0]; 
entries = 0;
cOld = cSI(1,1)+1;
for i = 1:rows
  
  c = cSI(i,1)+1; %actual cluster index
  
  if cOld ~= c
    %new cluster started in sorted matrix cSI --> store data of old cluster   
    collector{cOld,1} = {bTemp};
    collector{cOld,2} = {pLeftTemp};
    collector{cOld,3} = {pRightTemp};
    cOld = c;
    entries = 0;
  end
%  disp(sprintf('cluster %g, sample %g', n, i));
  index = 1+cSI(i,2); %index in pattern samples

  %ball
  if entries == 0
    bTemp = [samples(index,1), samples(index,2)];
%        disp(bTemp);
  else
    bTemp = vertcat( bTemp, [samples(index,1), samples(index,2)] );
  end
  for pn = 1:11
    if entries == 0
      %player team left
      pLeftTemp = [samples(index,pn*2+1), samples(index,pn*2+2)];
      %player team right
      pRightTemp = [samples(index,(pn+11)*2+1), samples(index,(pn+11)*2+2)];
      
      entries = 1;
    else
      %player team left
      pLeftTemp = vertcat( pLeftTemp, ...
              [ samples(index,pn*2+1), samples(index,pn*2+2)] );
      %player team right
      pRightTemp = vertcat( pRightTemp, ... 
              [ samples(index,(pn+11)*2+1), samples(index,(pn+11)*2+2)] );
    end
  end
end

%store data of last cluster
collector{cOld,1} = {bTemp};
collector{cOld,2} = {pLeftTemp};
collector{cOld,3} = {pRightTemp};


disp(sprintf('compute plot data end'));
%%%compute plot data end


disp(sprintf('plot graphs'));
%%%plot graphs

for n = 1:cCount
%  disp(sprintf('plot data for cluster %g', n));
  
  %plot cluster n
  figure(n); clf;
  set(gcf, 'WindowStyle','normal');
  hold on
   
  plot(collector{n, 2}{1}(:,1), collector{n, 2}{1}(:,2), 'b*'); %p left
  plot(collector{n, 3}{1}(:,1), collector{n, 3}{1}(:,2), 'g*'); %p right 
  plot(collector{n, 1}{1}(:,1), collector{n, 1}{1}(:,2), 'r*'); %ball
  axis([-1 1 -0.6 0.6]);
  grid;
  hold off
end

disp( ... 
  sprintf('plot all data for ball, p1 and p12 of all samples in figure %g' ...
              , cCount+1));
figure(cCount+1); clf;
set(gcf, 'WindowStyle','normal');
hold on
plot(Ball(:,1), Ball(:,2), 'r*');
plot(Player1(:,1), Player1(:,2), 'b*');
plot(Player2(:,1), Player2(:,2), 'g*');
axis([-1 1 -0.6 0.6]);
grid;
hold off

disp(sprintf('plot graphs end'));
%%%plot graphs end

disp(sprintf('end program'));
toc 
