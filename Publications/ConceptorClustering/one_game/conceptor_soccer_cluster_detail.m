%%%% first test script for conceptor learning on soccer logfile
%%%% F. Schmidsberger, Feb 28, 2017


%%% settings
prepareData = 1; % set 1 to reload csv files


tic
if isOctave
  page_screen_output(0);
  page_output_immediately(1);
end

disp(sprintf('start program'));

if prepareData == 1
  disp(sprintf('prepare data'));
  disp(sprintf('load xCollectorT_cl.csv'));
  cluster = load('data/xCollectorT_cl.csv');
  cSI = sortrows([cluster(:,end), cluster(:,1)] , 1);
  %[SO, ISO] = sortrows(cSI, 1);
  %SORTED = sortrows(cSI, 1);

  disp(sprintf('load MarliK_1-vs-Gliders2012_3.csv'));
  S = load ('data/MarliK_1-vs-Gliders2012_3.csv');
  washoutLength = 50; % how many initial updates per pattern are discarded
  %normalize selected positions
  samples = S(washoutLength+1:end,:)./60;

  save samples__cluster.m cSI;
  save samples__soccer.m samples;
  disp(sprintf('prepare data end'));
end

disp(sprintf('load data'));
load samples__cluster.m cSI;
load samples__soccer.m samples;
disp(sprintf('load data end'));


disp(sprintf('compute plot data'));
%%%plot graphs
pn1 = 1; %Number of player to plot
pn2 = 12; %Number of player to plot 
Ball = samples(:,1:2);
Player1 = samples(:,pn1*2+1:pn1*2+2);
Player2 = samples(:,pn2*2+1:pn2*2+2);

cCount = 1 + max(cSI(:,1));
%cCount = 4;
collector = cell(cCount, 3);

s_cSI=size(cSI);
rows = s_cSI(1,1);
disp(sprintf('sample count: %g, cluster count %g', rows, cCount));


%collect data for each cluster
%disp(sprintf('collect data for each cluster'));

%bTemp = [0,0];      % [x, y]
%pLeftTemp = [0,0];  % [x, y]
%pRightTemp = [0,0]; % [x, y]

bTemp = [0,0,0];      % [x, y, example index for this cluster]
pLeftTemp = [0,0,0];  % [x, y, example index for this cluster]
pRightTemp = [0,0,0]; % [x, y, example index for this cluster]

entries = 0;
example = 0;
sampleNumberOld = -2;
cOld = cSI(1,1)+1;
for i = 1:rows
  
  c = cSI(i,1)+1; %actual cluster index
  
  if cOld ~= c
    %new cluster started in sorted matrix cSI --> store data of old cluster   
    collector{cOld,1} = {bTemp};
    collector{cOld,2} = {pLeftTemp};
    collector{cOld,3} = {pRightTemp};
    cOld = c;
    example = 0;
    sampleNumberOld = -2;
    entries = 0;
  end
%  disp(sprintf('cluster %g, sample %g', n, i));
  index = 1+cSI(i,2); %index in pattern samples

  
  if sampleNumberOld + 1 < index
    example = example + 1;
  end
  sampleNumberOld = index;
  
  %ball
  if entries == 0
    bTemp = [samples(index,1), samples(index,2), example];
%        disp(bTemp);
  else
    bTemp = vertcat( bTemp, [samples(index,1), samples(index,2), example]);
  end
  for pn = 1:11
    if entries == 0
      %player team left
      pLeftTemp = [samples(index,2+pn), samples(index,3+pn), example];
      %player team right
      pRightTemp = [samples(index, 2+11+pn), samples(index,3+11+pn), example];
      
      entries = 1;
    else
      %player team left
      pLeftTemp = vertcat( pLeftTemp, ...
              [ samples(index,2+pn), samples(index,3+pn), example] );
      %player team right
      pRightTemp = vertcat( pRightTemp, ... 
              [ samples(index,2+11+pn), samples(index,3+11+pn), example] );
    end
  end
end

%store data of last cluster
collector{cOld,1} = {bTemp};
collector{cOld,2} = {pLeftTemp};
collector{cOld,3} = {pRightTemp};


disp(sprintf('compute plot data end'));
%%%compute plot data end


disp(sprintf('plot graphs'));
%%%plot graphs

for n = 1:cCount
  disp(sprintf('plot data for cluster %g of %g', n, cCount));
  
  %plot cluster n
  ballTemp =    collector{n, 1}{1};
  playerLTemp = collector{n, 2}{1};
  playerRTemp = collector{n, 3}{1};
  
  bSize = size(ballTemp);
  exampleCount = ballTemp(bSize(1,1), bSize(1,2));
  patternCount = bSize(1,1);

  
  figure(n); clf;
  set(gcf, 'WindowStyle','normal');
  
  subplot((exampleCount + 1)/2,2,1);
  hold on
  plot(collector{n, 2}{1}(:,1), collector{n, 2}{1}(:,2), 'b*'); %p left
  plot(collector{n, 3}{1}(:,1), collector{n, 3}{1}(:,2), 'g*'); %p right 
  plot(collector{n, 1}{1}(:,1), collector{n, 1}{1}(:,2), 'r*'); %ball
  axis([-1 1 -0.6 0.6]);
%  axis([-1.5 1.5 -1 1]);
  grid;
  hold off
  
  for e = 1:exampleCount
    bTemp = [0,0];      % [x, y]
    pLeftTemp = [1,0,0];  % [x, y]
    pRightTemp = [1,0,0]; % [x, y]
    entry = 0;
    for p = 1:patternCount
      if ballTemp(p, 3) == e
        if entry == 0
          entry = 1;
          bTemp = [ballTemp(p,1), ballTemp(p,2)];
          pLeftTemp =  [1, playerLTemp(1+((p-1)*11),1), ...
                           playerLTemp(1+((p-1)*11),2)];
          pRightTemp = [1, playerRTemp(1+((p-1)*11),1), ...
                           playerRTemp(1+((p-1)*11),2)];
                        
          for plNumber = 2:11
            pLeftTemp = vertcat(pLeftTemp,                              ...
                                  [plNumber,                            ...
                                    playerLTemp(plNumber+((p-1)*11),1), ...
                                    playerLTemp(plNumber+((p-1)*11),2)  ...
                                  ]                                     ...
                                );
            pRightTemp = vertcat(pRightTemp,                            ...
                                  [plNumber,                            ...
                                    playerRTemp(plNumber+((p-1)*11),1), ...
                                    playerRTemp(plNumber+((p-1)*11),2)  ...
                                  ]                                     ...
                                );                              
          end
        else
          bTemp = vertcat(bTemp, [ballTemp(p,1), ballTemp(p,2)]);
          for plNumber = 1:11
            pLeftTemp = vertcat(pLeftTemp,                              ...
                                  [plNumber,                            ...
                                    playerLTemp(plNumber+((p-1)*11),1), ...
                                    playerLTemp(plNumber+((p-1)*11),2)  ...
                                  ]                                     ...
                                );
            pRightTemp = vertcat(pRightTemp,                            ...
                                  [plNumber,                            ...
                                    playerRTemp(plNumber+((p-1)*11),1), ...
                                    playerRTemp(plNumber+((p-1)*11),2)  ...
                                  ]                                     ...
                                );                              
          end
        end
      end
    end
    
    subplot((exampleCount + 1)/2,2,1+e);
    hold on
    
    playerStepsSize = size(pLeftTemp);

    for plNumber = 1:11
      entry = 0;
      left = [0,0];
      right = [0,0];
      for step = 1:playerStepsSize(1,1)
        if pLeftTemp(step,1) == plNumber
          
          if entry == 0
            entry = 1;
            left = [pLeftTemp(step,2), pLeftTemp(step,3)];
            right = [pRightTemp(step,2), pRightTemp(step,3)];
          else
            left  = vertcat(left, [pLeftTemp(step,2),  pLeftTemp(step,3)] );
            right = vertcat(right,[pRightTemp(step,2), pRightTemp(step,3)] );
          end
        end
      end
      
      plot(right(:,1), right(:,2), 'g*-'); %p right
      plot(left(:,1), left(:,2), 'b*-'); %p left
    end

    
    plot(bTemp(:,1), bTemp(:,2), 'r*-'); %ball
    axis([-1 1 -0.6 0.6]);
%    axis([-1.5 1.5 -1 1]);
    grid;
    hold off
  end
end

disp( ... 
  sprintf('plot all data for ball, p1 and p12 of all samples in figure %g' ...
              , cCount+1));
figure(cCount+1); clf;
set(gcf, 'WindowStyle','normal');
hold on
plot(Ball(:,1), Ball(:,2), 'r*');
plot(Player1(:,1), Player1(:,2), 'b*');
plot(Player2(:,1), Player2(:,2), 'g*');
axis([-1 1 -0.6 0.6]);
%axis([-1.5 1.5 -1 1]);
grid;
hold off

disp(sprintf('plot graphs end'));
%%%plot graphs end

disp(sprintf('end program'));
toc 
