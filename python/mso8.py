#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 22 12:16:33 2020

@author: oliver
"""

import numpy as np
from prnn import PrNN

class MSO:
    def __init__(self):
        self.alpha = np.array([0.2, 0.311, 0.42, 0.51, 0.63, 0.74, 0.85, 0.97])
        self.m0 = 0
        self.n0 = 0
        self.ph = None
        
    def signal(self, n = 1000, m = 8):
        if self.ph is None or self.ph.shape[1] < n:
            m1 = self.alpha.shape[0] + 1
            self.ph = np.zeros((m1, n))
            for k in range(1,m1):
                self.ph[k,:] = np.sin([self.alpha[k-1] * t for t in range(n)])
        if self.m0 != m or self.n0 < n:
            self.ph[0,:] = 0
            for k in range(0,m):
                self.ph[0,:] += self.ph[k+1,:]
            self.m0 = m
            self.n0 = n
          
        return self.ph[0,0:n]
    

mso = MSO()

reps = 100

signal = mso.signal(n = 1000, m = 8)

d = 1
n = [130, 150, 200]
m = range(70, 201, 10)

data = np.zeros((len(n), len(m), reps))

for i in range(len(n)):
    for j in range(len(m)):
        for k in range(reps):
            rnn = PrNN(m[j], d)
            out, err, A, J, Y, W, X = rnn.predict(signal[0:n[i]], 0, m[j], theta=0.5, delta=0)
            data[i,j,k] = len(J)
