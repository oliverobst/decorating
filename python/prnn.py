#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 16 20:43:48 2020

@author: oliver
"""

import numpy as np
from numpy import linalg as LA
from scipy.linalg import block_diag, pinv, pinv2
import matplotlib.pyplot as plt 
  
class PrNN:
    
    def __init__(self, Nres, d=0):
        self.initialise(Nres, d)
        
    def initialise(self, Nres, d):
        self.d = d
        self.Nres = Nres
        self.N = int(Nres + d)
        # recurrent weight matrix: we have Nres reservoir + d "input" nodes
        self.W = np.zeros((self.N, self.N))
        # init input weights
        self.W[d:self.N,0:d] = np.random.normal(size=(Nres,d)) / np.sqrt(d)
        # create the "reservoir" matrix
        self.W[d:self.N, d:self.N] = self.reservoir(Nres)
        
    def reservoir(self, N):
        """Generate a random recurrent weight matrix with spectral radius 1.

        Returns
        -------
        W : the NxN reservoir matrix.

        """
        
        if N > 1:
            while True:
                try:
                    W = np.random.normal(size = (N,N))
                    w, v = LA.eig(W)
                    lambda0 = abs(max(w))
                    W = W / lambda0
                    break
                except LA.LinAlgError:
                    pass            
        else:
            W = np.ones((N,N))
            
        return W
            
    def balanced(self, N=None):
        """Generate a balanced start vector.
        
        Each entry is 1/sqrt(N).

        Parameters
        ----------
        N : int, optional
            Specifies the size of the RNN. The default None means
            to use the given size.

        Returns
        -------
        Start vector with N elements.

        """
        
        if N is None:
            N = self.N
        return np.ones(N) / max(np.sqrt(N), 1.0)
    
    def cluster(self, values, delta=-1):
        """Cluster values based on their distances.

        Parameters
        ----------
        values : a list of eigenvalues (complex numbers)
            The list of eigenvalues to cluster.
        delta : a number, optional
            Distance value for cluster centroids. The default of -1
            means to automatically compute a min distance by using 
            an equidistant spread on the unit circle.

        Returns
        -------
        centr : array of complex values
            The cluster centroids.
        multi : TYPE
            DESCRIPTION.

        """
        # initialise
        N = len(values)
        if delta < 0:
            delta = np.pi / N

        centr = values.copy()   # set cluster centroids
        multi = np.ones(N, dtype=int)      # cluster sizes
        
        if delta == 0:          # in this case we're done
            return (centr, multi)
        
        L = list(range(N))      # links to cluster centroids
        
        for i in range(2,N):
            ii = i              # link to new entry
            for j in range(i-1):
                jj = L[j]
                while multi[jj] == 0:
                    jj = L(jj)
                if abs(values[i] - values[j]) < delta and ii != jj:
                    centr[jj] = multi[jj] * centr[jj] + multi[ii] * centr[ii]
                    multi[jj] += multi[ii]
                    centr[jj] /= multi[jj]
                    multi[ii] = 0
                    L[ii] = jj
                    ii = jj
        
        centr = centr[multi != 0]
        multi = multi[multi != 0]
        
        return (centr, multi)
 
    def clean(self, centr, multi):
        """Filter out important eigenvalues.
        
        This removes eigenvalues with negative imaginary part.
        The count for eigenvalues with positive imaginary part is doubled. 
        

        Parameters
        ----------
        centr : Array of complex values
            A list of eigenvalues.
        multi : Array of numbers (counts)
            Cluster sizes.

        Returns
        -------
        lbda : Array of complex values
            A reduced list of eigenvalues.
        mout : Array of numbers (counts)
            The new cluster sizes.

        """
        index = centr.imag >= 0 # find values with non-negative imaginary path
        lbda = centr[index]
        mout = multi[index]
        mout[lbda.imag > 0] *= 2
        return (lbda, mout)
    
    def block(self, lbda, m):
        """Build a real Jordan block for eigenvalue lambda of multiplicity m.

        Possibly includes complex conjugated eigenvalues.        

        Parameters
        ----------
        lbda : complex number
            An eigenvalue.
        m : int
            multiplicity.

        Returns
        -------
        J : A matrix
            Returns the jordan block as matrix.

        """
        
        typ = 1 + np.iscomplex(lbda)
        m = int(m/typ) # proper complex numbers count twice (complex conjugated pair)
        J = np.real(lbda) * np.eye(int(typ*m)) # main diagonal
        
        if typ == 2: # eigenvalue is complex
            D = np.diag(np.tile([lbda.imag, 0], (1, m)).squeeze()[0:-1])
            J[0:-1,1:] += D    # upper diagonal
            J[1:,0:-1] += -D   # lower diagonal
            
        if m > 1: 
            J[0:-typ,typ:] += np.eye(typ * (m-1))
        
        return J
        
    def jormat(self, lbda, multi):
        """create real Jordan matrix of size NxN consisting of K jordan blocks

        Usues list of eigenvalues lbda with corresponding multiplicities multi.
        
        Parameters
        ----------
        lbda : List of complex values
            Eigenvalues.
        multi : Array of ints
            Multiplicities of eigenvalues.

        Returns
        -------
        J : Matrix
            The block diagonal jordan matrix.
        N : int
            Matrix size.
        K : int
            Number of real jordan blocks.

        """
        
        J = None
        N = 0 # matrix size
        K = len(lbda) # number of real jordan blocks
        
        for i in range(K):
            m = multi[i]
            if J is None:
                J = self.block(lbda[i], m)
            else:
                J = block_diag(J, self.block(lbda[i], m))
            N += m
            
        return (J,N,K)
    
    def rmse(self, inp, out, mode=1, flag=1):
        """Different ways to compute the root mean square error.
        
        Parameters
        ----------
        inp : 2-d matrix
            a sequence of d-dimensional target values.
        out : 2-d matrix
            a sequence of d-dimensional actual values.
        mode : int [0,1,2], optional
            0: rmse per row, 1: sum up per columns, 2: average per column. 
               The default is 1.
        flag : int [0,1,2], optional
            0: sum up rows, 1: averge rows, 
            2: average rows, divide by row variance. The default is 1.

        Returns
        -------
        The computed RMSE, a 1-d array (with one or more values).

        """
        
        result = (out-inp)**2
        
        # if flag == 0 we don't do anything here 
        if flag == 1:       # sum up the columns (l2 distance)
            result = np.expand_dims(np.sum(result, axis=0), axis=0)
        elif flag == 2:  # compute column means
            result = np.expand_dims(np.mean(result, axis=0), axis=0)
            
        if mode == 0:
            result = np.sum(result, axis=1)
        elif mode == 1:
            result = np.mean(result, axis=1)
        elif mode == 2:
            result = np.mean(result, axis=1) / np.var(inp, ddof=1, axis=1)
            
        return np.sqrt(result)
            
    def compute(self, W, X, m=10, S=None):
        """Run data through PrNN.
        
        compute has two modes: input-receiving and output-generating.
        If both modes are used, the PrNN will receive input first, and 
        then generate output.

        Parameters
        ----------
        W : a square matrix
        X : a 1-d array (or Nx1 matrix)
            The initial state of the PrNN.
        m : int, optional
            Number of output-generating steps. The default is 10.
        S : a 2-d matrix, optional
            The d-dimensional input time series, (d x (n+1)), over n+1 steps.   
            The default is None, in which case S is set to the initial 
            state X, i.e., of size (N x 1).

        Returns
        -------
        Y : a 2-d matrix, (N x (n+1+m)).
            The output has the same dimensionality as the PrNN, and is 
            computed over n+1+m steps (i.e., contains all n+1 input steps
                                       and the m predictions)
        """
        
        if len(X.shape) > 1: # if x is a matrix
            X = X.squeeze()  # turn x into 1-d vector
        if S is None:
            S = np.expand_dims(X, axis=1) # make sure S is a matrix
    
        d = S.shape[0]     # input matrix represents d-dimensional f(0)..f(n)
        n = S.shape[1] - 1 # number steps - 1 (i.e., n is highest index in S)
        N = X.shape[0]     # total number of PrNN units (represented by X)
        Y = np.zeros((N, n+1+m))
        Y[:,0] = X
        
        # input receiving mode
        Y[0:d, 0:n+1] = S
        if n > 0:
            I = range(d, N)
            M = W[I,:]
            for k in range(1,n+1):
                Y[I,k] = np.dot(M,Y[:,k-1])
                
        # output generating mode
        if m > 0:
            X = Y[:,n]
            for k in range(n+1,n+m+1):
                X = np.dot(W,X)
                Y[:,k] = X
                
        return Y

    def predict(self, S, m, Nres=-1, theta=-np.inf, delta=0, irange=None):
        """
        

        Parameters
        ----------
        S : d x (n+1) matrix
            Input data, with d dimensions, n+1 steps.
        m : int
            Number of steps to predict.
        Nres : int, optional
            Number of reservoir units. The default is -1, which means
            to auto-compute initial reservoir size.
        theta : TYPE, optional
            DESCRIPTION. The default is -np.inf.
        delta : TYPE, optional
            DESCRIPTION. The default is 0.
        irange : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        out : TYPE
            DESCRIPTION.
        err : TYPE
            DESCRIPTION.
        A : TYPE
            DESCRIPTION.

        """
        
        # some checks
        if len(S.shape) == 1: # if S is 1d-vector
            S = np.expand_dims(S, axis=0) # make sure S is a matrix

        # Initialisation
        d = S.shape[0]     # number of input dimensions
        n = S.shape[1] - 1 # number of steps - 1 (highest index in S)
        if Nres < 0:          # auto-determine initial size?
            Nres = n - d

        # repeat generating networks until error is small enough
        while True:
            self.initialise(Nres, d)
            X = np.zeros((d+Nres, n+1))
            X[d:d+Nres, 0] = self.balanced(Nres) # start() in our octave code
            
            # we drive through input in input receiving mode
            X = self.compute(self.W, X[:,0], m=0, S=S)
            
            # learn output weights using a lin reg on state collection matrix
            Yout = S[:,1:n+1]     # output: one step-ahead prediction
            self.W[0:d,:] = np.dot(Yout, pinv2(X[:,0:n]))
            
            # real Jordan decomposition
            if irange is None:
                irange = range(d)
            # cluster eigenvals
            centr, multi = self.cluster(LA.eigvals(self.W), delta)
            # compact representation
            lbda, multi = self.clean(centr, multi) 
            # construct real Jordan matrix, determine mapping matrix
            J,N,K = self.jormat(lbda, multi)

            # use network in output generating mode
            Y = self.compute(J, self.balanced(N), n)
            A = np.dot(X, pinv2(Y))[irange,:]
            
            # initialisation
            inp = X[irange,:]
            out = np.dot(A,Y)    # predicted sequence
            err = self.rmse(inp, out) # original error
            if err[0] < abs(theta):
                break
            
        # network size reduction
        if theta > 0:
            # compute error for each component
            error = np.zeros(K) 
            pos = 0  # position in jordan matrix
            for kk in range(K):
                mk = multi[kk]
                index = list(range(pos)) + list(range(pos+mk,N))
                JJ = J.take(index, axis=0).take(index, axis=1)
                YY = self.compute(JJ, self.balanced(N-mk), n)
                AA = np.dot(X, LA.pinv(YY))[irange,:]
                outx = np.dot(AA, YY)
                error[kk] = self.rmse(inp, outx)
                pos += mk
                
            # re-order jordan components
            ord = np.argsort(error)[::-1]
            error = error[ord]
            lbda = lbda[ord]
            multi = multi[ord]
            J,N,K = self.jormat(lbda, multi)
            cumul = np.cumsum(multi, axis=0)
        
            # now we can omit some components as long as error remains small
            k1 = 0
            k2 = K
            while k1<k2:
                k = int((k1+k2) / 2)
                NN = cumul[k]
                JJ = J.take(range(NN), axis=0).take(range(NN), axis=1)
                YY = self.compute(JJ, self.balanced(NN), n)
                AA = np.dot(X, LA.pinv(YY))[irange,:]
                outx = np.dot(AA, YY)
                errx = self.rmse(inp, outx)
                if errx[0] < theta:
                    out = outx
                    err = errx
                    A = AA.copy()
                    J = JJ
                    Y = YY
                    N = NN
                    k2 = k
                else:
                    k1 = k + 1
                
        if m > 0:
            out = np.concatenate((out[:,-1], np.dot(A, self.compute(Y[:,-1]), m)), axis=1)
        
        return out, err, A, J, Y, self.W, X