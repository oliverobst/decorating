# Decorating

## Deep Conceptors for Temporal Data Mining

With this project, we provide the software for LRNNs (linear recurrent neural
networks). The software has been implemented by the authors in the scientific
programming language Octave (see <http://www.octave.org>) and tested with version
4.4.2 under Linux. The main routines can be found in "examples/prediction".
Other examples can be found there, too. For further details of LRNNs and their
properties, the interested reader is referrred to the following paper.

The directory python contains a subset of the Octave implementation translated to
Python3. For all comparisons, the Octave implementation is the reference
implementation, and only the Octave results should be used for now.


## Attributions

The relevant paper for the software and case studies is at <http://arxiv.org/abs/1802.03308>:

Frieder Stolzenburg, Sandra Litz, Olivia Michael und Oliver Obst. The power of
linear recurrent neural networks. CoRR – Computing Research Repository
abs/1802.03308, Cornell University Library, 2018. Latest revision 2022.


## Acknowledgements

The research reported here has been supported by the German Academic Exchange
Service (DAAD) by funds of the German Federal Ministry of Education and Research
(BMBF) in the Programmes for Project-Related Personal Exchange (PPP) under grant
no. 57319564 and Universities Australia (UA) in the Australia-Germany Joint
Research Cooperation Scheme within the project Deep Conceptors for Temporal Data
Mining (Decorating).


## Licenses

The code that we make available with the Decorating project is released under 
[GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html): https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

